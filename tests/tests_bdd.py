import unittest
import sqlite3
import logging
import csv 

from app import InsertLine

class TestBDDFunction(unittest.TestCase):

    def setUp(self):
        self.connexion = sqlite3.connect('bdd_tdd.bd')
        cursor = self.connexion.cursor()
        cursor.execute('CREATE TABLE IF NOT EXISTS Automobiles(address TEXT, carrosserie TEXT, categorie TEXT, couleur TEXT, cylindree INT, date_immat TEXT, denomination TEXT, energy TEXT, firstname TEXT, immat TEXT PRIMARY KEY, marque TEXT, name TEXT, places INT, poids INT, puissance INT, type_variante_version TEXT, vin TEXT)')
        self.connexion.commit()

    def tearDown(self):
        self.connexion.close()
        self.connexion = None
        import os
        os.remove('bdd_tdd.bd')

    def insert_sample(self):
        pass # insert some data


    def test_InsertLine(self):

        # Permet l'insertion de data dans la bdd test
        with open("test1.csv") as csvfile:
            reader = csv.reader(csvfile, delimiter='|') 
            for row in reader:
                InsertLine(row, self.connexion)
                self.connexion.commit()

        # Permet de récupérer la data de la bdd test pour vérifier l'insertion
        cursor = self.connexion.cursor()
        cursor.execute("SELECT * FROM Automobiles")
        rows_bd = cursor.fetchall()

        for row_bd in rows_bd:
            self.assertEqual(row_bd, ('a', 'b', 'c', 'd', '1', 'f', 'g', 'h', 'i', 'j', 'k', 'l', '2', '3', '4', 'p', 'q'))
    

    def test_UpdateLine(self):

        # Permet l'insertion de data dans la bdd test
        with open("test1.csv") as csvfile:
            reader = csv.reader(csvfile, delimiter='|') 
            for row in reader:
                InsertLine(row, self.connexion)
                self.connexion.commit()

        # Permet l'update de data dans la bdd test
        with open("test2.csv") as csvfile:
            reader = csv.reader(csvfile, delimiter='|') 
            for row in reader:
                InsertLine(row, self.connexion)
                self.connexion.commit()

        # Permet de récupérer la data de la bdd test pour vérifier l'update
        cursor = self.connexion.cursor()
        cursor.execute("SELECT * FROM Automobiles")
        rows_bd = cursor.fetchall()

        for row_bd in rows_bd:
            self.assertEqual(row_bd, ('aaa', 'b', 'c', 'd', '1', 'f', 'g', 'h', 'i', 'j', 'k', 'l', '2', '3', '4', 'p', 'q'))