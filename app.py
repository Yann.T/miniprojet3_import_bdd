import sqlite3
import logging
import csv
import argparse
import os.path
from os import path

"""
Mini-Projet N°3 Python
======================

Ce mini-projet à pour but d'utiliser Sqlite3, le logging et un csv pour insérer ou 
update des données dans une BDD.

Programme réalisé par TRICOT Yann & CRAMEZ Théo
"""

# Formatage des loggings pour avoir l'odre et les messages que l'on souhaite afficher
logging.basicConfig(filename='logfile.log', format='%(asctime)s %(levelname)s %(message)s', level=logging.DEBUG)
logging.info("Program Started")

# Permet la connexion à la BDD et la création d'un cursor permettant de réaliser des 
# commandes sur celle-ci
connexion = sqlite3.connect('bdd_auto.bd')
logging.info("Connection successful with the DateBase")
c = connexion.cursor()

# Commande permettant la création d'une table dans la BDD si celle-ci n'y existe pas
c.execute('CREATE TABLE IF NOT EXISTS Automobiles(address TEXT, carrosserie TEXT, categorie TEXT, couleur TEXT, cylindree INT, date_immat TEXT, denomination TEXT, energy TEXT, firstname TEXT, immat TEXT PRIMARY KEY, marque TEXT, name TEXT, places INT, poids INT, puissance INT, type_variante_version TEXT, vin TEXT)')
# Le commit permet l'envoi vers la BDD des commandes réalisé précédement
connexion.commit()

def InsertLine(row, connexion):
    """
    Fonction InsertLine :
    =====================
    % IN :[row] <- row étant la ligne courante lu du csv
    % OUT:[/]   <- après l'éxécution de cette fct la table 
                   'Automobiles' est modif
    1. On partitionne la ligne en une liste ordonnée de la data à insérer
    2. On éxécute la commande permettant l'insertion ou l'update d'un
       enregistrement dans la table 'Automobiles'
    3. On commit nos commandes
    """
    donnee_BDD = [str(row[0]), str(row[1]), str(row[2]), str(row[3]), int(row[4]), 
        str(row[5]), str(row[6]), str(row[7]), str(row[8]), str(row[9]), str(row[10]), 
        str(row[11]), int(row[12]), int(row[13]), int(row[14]), str(row[15]), str(row[16])]

    c.execute('''INSERT OR REPLACE INTO Automobiles (address, carrosserie, categorie, couleur, 
    cylindree, date_immat, denomination, energy, firstname, immat, marque, name, places, poids, puissance, 
    type_variante_version, vin) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);''', donnee_BDD)

    connexion.commit()

if __name__ == "__main__":

    #Code permettant la mise en place des informations par rapport aux argument a fournir
    parser=argparse.ArgumentParser(description="Programme d'insertion de data d'un csv vers une BDD Sqlite.")
    parser.add_argument("cheminFichierCSV", help="Chemin vers le fichier CSV correspondant à la data à extraire", type=str)
    args = parser.parse_args()

    #permet de vérifier si le fichier passé en paramètre existe
    if not os.path.isfile(args.cheminFichierCSV) or not args.cheminFichierCSV.lower().endswith('.csv'):
        logging.warning("FILE ERROR") 
        logging.warning("Program brutally stopped")
        os.sys.exit(1)

    # On ouvre le csv en 'Lecture seule'
    with open('auto.csv', 'r', newline='') as csvfile:
        logging.info("CSV file opened")

        # On lit le csv ligne par ligne avec en délimiteur '|'
        csvreader = csv.reader(csvfile, delimiter='|')
        # Booleen permettant de sauter le header du csv
        first_line = True

        # Foreach lisant ligne par ligne le csv et appelant la FCT InsertLine en lui 
        # passant en paramètre la ligne courante découpée
        for row in csvreader:
            if first_line == True :
                first_line = False
                logging.info("Insertion of Data started")
            else:
                InsertLine(row, connexion) #ici on appelle la fct INSERT
        logging.info("Insertion of Data finished")
    logging.info("CSV file closed")

    # On clôture la connexion avec la BDD
    connexion.close()
    logging.info("Connection with the DataBase closed successfully")
    logging.info("Program Stopped")

